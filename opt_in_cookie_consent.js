/**
 * @file
 * Javascript file for Drupal Module: Opt-in cookie consent
 * Handles all javascript actions for the module
 */

(function ($) {
  // Behavior to initialize cookie.
  Drupal.behaviors.initCookiebar = {
      attach: function (context) {

        // First, set up cookies interface.
        $.rocookies.init();

        var settings = {
          cookiename: Drupal.settings.opt_in_cookie_consent.cookiename,
          cookievalues: Drupal.settings.opt_in_cookie_consent.cookievalues,
          cookieurl: Drupal.settings.opt_in_cookie_consent.cookieurl,
          cookielog: 'cookielog',
          confirmation_enabled: Drupal.settings.opt_in_cookie_consent.confirmation_enabled
        };

        settings[Drupal.settings.opt_in_cookie_consent.language] = {
          question: Drupal.settings.opt_in_cookie_consent.question,
          change: Drupal.settings.opt_in_cookie_consent.change,
          accept: Drupal.settings.opt_in_cookie_consent.accept,
          deny: Drupal.settings.opt_in_cookie_consent.deny,
          close: Drupal.settings.opt_in_cookie_consent.close
        };

        settings.callback = function (ccResult) {
          // If visitor agreed, we need to document this by setting a cookie and logging an URL with a unique code for legal reasons.
          // If the visitor disagreed, we just set a 'deny' cookie and request the image (cookieless!) without a unique code.
          var   agent = navigator.userAgent.hashCode(),
                now = new Date(),
                timestamp = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()),
                uniqueid = timestamp + agent.toString(),
                lifespan = $.rocookiebar.settings['lifespan'] || 5 * 365,
                choice = $.rocookiebar.settings['cookievalues'][ccResult];

          if (ccResult == 'accept'){
            consent = choice + "." + uniqueid;
          } else {
            consent = choice;
          }
          var urlparam = choice + "." + uniqueid;

          // Remember choice in cookie.
          $.rocookies.create($.rocookiebar.settings['cookiename'],consent,$.rocookiebar.settings['lifespan']);

          if (!$.rocookies.show_confirmation){
            if (ccResult == 'accept' || ccResult == 'deny'){
            // let drupal know if choice is logged
            $.rocookies.create($.rocookiebar.settings['cookielog'],true,1);
            if ($.rocookiebar.settings['confirmation_enabled'] == true){
              $.rocookies.create('show_confirmation', ccResult, 1);
            }
            location.href = "?cookie=" + urlparam;
          }
        }
      }

      // Start cookiebar with the configured settings.
      $.rocookiebar.init(settings);

      // Open change preference screen if visitor clicks change preferences link
      $('.cc-change-preferences').click(function(evt)
      {
        evt.preventDefault();
        $.rocookiebar.build('change');
      });

      if ($.rocookies.show_confirmation){
        $.rocookiebar.build('change');
        $.rocookiebar.thankyou($.rocookies.show_confirmation);
        $.rocookiebar.settings.callback($.rocookies.show_confirmation);
        $.rocookies.erase('show_confirmation');
      }

      // If a visitor returns after making a choice, you can test for the cookie as follows.
      var ck = $.rocookies[$.rocookiebar.settings['cookiename']] || '';
      if (ck.indexOf($.rocookiebar.settings['cookievalues']['accept']) > -1) {

      }
    }
  };
}(jQuery));

/**
 * Reads content of a cookie
 */

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

/*
 * Finds if part of a string is in a string
 */

function strstr (haystack, needle) {
  var pos = 0;
  haystack += '';
  pos = haystack.indexOf(needle);
  if (pos == -1) {
    return false;
  }
  else {
    return true;
  }
}
