<?php

/**
 * @file
 * Hooks provided by Opt-in cookie consent to use by submodules.
 */

/**
 * @addtogroup hooks
 */

/**
 * Hook for unsetting cookies.
 *
 * Submodules may use this hook to unset cookies if the visitor 
 * did not accept the cookie consent.
 *
 * @return array
 *   An array containing the names of the cookies that need to be
 *   included in the cookie consent functionality.
 */
function hook_opt_in_cookie_consent_cookies() {
  return array('_somecookiename', '_someothercookiename');
}
