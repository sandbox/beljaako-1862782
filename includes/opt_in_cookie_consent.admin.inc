<?php

/**
 * @file
 * Cookie Consent admin functions.
 */

/**
 * Settings page callback function.
 */
function opt_in_cookie_consent_settings_form($form, &$form_state) {
  $form = array();

  // General settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 0,
  );
  $form['general']['opt_in_cookie_consent_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Rijksoverheid Cookie Opt-in'),
    '#description' => t('Enable the Rijksoverheid Cookie Opt-in functionality.'),
    '#default_value' => variable_get('opt_in_cookie_consent_enabled', FALSE),
    '#weight' => 0,
    '#disabled' => _opt_in_cookie_consent_init_library() == TRUE ? FALSE : TRUE,
  );
  $form['general']['opt_in_cookie_consent_log_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Rijksoverheid Cookie Opt-in log'),
    '#description' => t('Enable logging of all responses to the Drupal database. If you disable this, you will have to implement other ways of keeping the responses to conform to the Dutch cookie law. You can keep all apache access logs for example, but be aware that you have to keep them for at least five years.'),
    '#default_value' => variable_get('opt_in_cookie_consent_log_enabled', TRUE),
    '#weight' => 5,
  );
  $form['general']['opt_in_cookie_consent_confirmation_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Rijksoverheid Cookie Opt-in confirmation message'),
    '#description' => t('Enable confirmation message when a visitor accepts or denies the cookie consent.'),
    '#default_value' => variable_get('opt_in_cookie_consent_confirmation_enabled', TRUE),
    '#weight' => 5,
  );

  // Text settings.
  $form['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookiebar text settings'),
    '#description' => t('Texts used in the cookiebar.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => 5,
  );
  $form['text']['opt_in_cookie_consent_close'] = array(
    '#type' => 'textfield',
    '#title' => t('Close link'),
    '#description' => t('The close link text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_close', '<a href="#main">Close this menu bar</a>'),
    '#weight' => 0,
  );
  $form['text']['question'] = array(
    '#type' => 'fieldset',
    '#title' => t('Question texts'),
    '#description' => t('Texts that are used when asking for consent.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 5,
  );
  $form['text']['question']['opt_in_cookie_consent_question_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_question_title', 'Cookies on Example.com'),
    '#weight' => 0,
  );
  $form['text']['question']['opt_in_cookie_consent_question_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Intro'),
    '#description' => t('The introduction text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_question_intro', 'May Example.com place <a href="/cookies">cookies</a> on your computer to make the site easier for you to use?'),
    '#weight' => 5,
  );
  $form['text']['question']['opt_in_cookie_consent_question_learn_more'] = array(
    '#type' => 'textarea',
    '#title' => t('Learn more links'),
    '#description' => t('The learn more links text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_question_learn_more', '<p>I first want to know more ...</p><ul><li><a href="/cookies">What do cookies do?</a></li><li><a href="/privacy">What about my privacy?</a></li></ul>'),
    '#weight' => 10,
  );
  $form['text']['change'] = array(
    '#type' => 'fieldset',
    '#title' => t('Change texts'),
    '#description' => t('Texts that are used when changing consent.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );
  $form['text']['change']['opt_in_cookie_consent_change_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_title', 'Changing your cookie preferences on Example.com'),
    '#weight' => 0,
  );
  $form['text']['change']['opt_in_cookie_consent_change_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Intro'),
    '#description' => t('The introduction text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_intro', ''),
    '#weight' => 5,
  );
  $form['text']['change']['opt_in_cookie_consent_change_learn_more'] = array(
    '#type' => 'textarea',
    '#title' => t('Learn more links'),
    '#description' => t('The learn more links text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_learn_more', '<p>I first want to know more ...</p><ul><li><a href="/cookies">What do cookies do?</a></li><li><a href="/privacy">What about my privacy?</a></li></ul>'),
    '#weight' => 10,
  );
  $form['text']['accept'] = array(
    '#type' => 'fieldset',
    '#title' => t('Accept texts'),
    '#description' => t('Texts that are used when consent is given.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 15,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button'),
    '#description' => t('The accept button text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_accept_button', 'Yes, I accept cookies'),
    '#weight' => 0,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_extras'] = array(
    '#type' => 'textarea',
    '#title' => t('Extras'),
    '#description' => t('The extra button text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_accept_extras', '<ul><li>Example.com will collect anonymous information about your visits to help improve the website.</li></ul>'),
    '#weight' => 5,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_accept_title', 'You have chosen to accept cookies from Example.com'),
    '#weight' => 10,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Intro'),
    '#description' => t('The introduction text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_accept_intro', 'Thank you for permitting Example.com to place cookies on your computer. You can always <a href="#" class="ck-change">change your mind</a>.'),
    '#weight' => 15,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_current'] = array(
    '#type' => 'textarea',
    '#title' => t('Current'),
    '#description' => t('The current selection text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_accept_current', 'You now accept cookies from Example.com. You can change your preference below. May Example.com continue to place <a href="/cookies">cookies</a> on your computer to make the site easier for you to use?'),
    '#weight' => 20,
  );
  $form['text']['accept']['opt_in_cookie_consent_accept_learn_more'] = array(
    '#type' => 'textarea',
    '#title' => t('Learn more links'),
    '#description' => t('The learn more links text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_learn_more', '<ul><li><a href="/cookies">What do cookies do?</a></li><li><a href="/privacy">What about my privacy?</a></li></ul>'),
    '#weight' => 25,
  );
  $form['text']['deny'] = array(
    '#type' => 'fieldset',
    '#title' => t('Deny texts'),
    '#description' => t('Texts that are used when consent is not given.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 20,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Button'),
    '#description' => t('The deny button text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_button', 'No, I don\'t accept cookies'),
    '#weight' => 0,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_extras'] = array(
    '#type' => 'textarea',
    '#title' => t('Extras'),
    '#description' => t('The extra button text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_extras', '<ul><li>Example.com will not use cookies to collect anonymous information about your visits.</li></ul>'),
    '#weight' => 5,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_title', 'Example.com respects your choice'),
    '#weight' => 10,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Intro'),
    '#description' => t('The introduction text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_intro', 'This website will not use cookies to collect information about your visits.'),
    '#weight' => 15,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_current'] = array(
    '#type' => 'textarea',
    '#title' => t('Current'),
    '#description' => t('The current selection text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_current', 'You do not accept cookies from Example.com. You can change this preference below. May Example.com place cookies on your computer to make the site easier for you to use?'),
    '#weight' => 20,
  );
  $form['text']['deny']['opt_in_cookie_consent_deny_learn_more'] = array(
    '#type' => 'textarea',
    '#title' => t('Learn more links'),
    '#description' => t('The learn more links text.'),
    '#default_value' => variable_get('opt_in_cookie_consent_deny_learn_more', '<ul><li><a href="/cookies">What do cookies do?</a></li><li><a href="/privacy">What about my privacy?</a></li><li><a href="#" class="ck-change">Change your cookie preferences</a></li></ul>'),
    '#weight' => 25,
  );

  // Cookiebar color settings.
  $form['color_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rijksoverheid Cookie Consent color settings'),
    '#description' => t('Settings for the colors of the Rijksoverheid Cookie Consent bar.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );
  $form['color_settings']['opt_in_cookie_consent_color_settings_background'] = array(
    '#type' => 'select',
    '#title' => t('Background color'),
    '#options' => array(
      'dark-brown' => t('Dark-brown'),
      'purple' => t('Purple'),
      'dark-green' => t('Dark-green'),
      'ruby' => t('Ruby'),
      'red' => t('Red'),
      'violet' => t('Violet'),
      'moss-green' => t('Moss-green'),
      'brown' => t('Brown'),
      'green' => t('Green'),
      'azure' => t('Azure'),
      'light-blue' => t('Light-blue'),
      'pink' => t('Pink'),
      'yellow' => t('Yellow'),
      'dark-yellow' => t('Dark-yellow'),
      'mint-green' => t('Mint-green'),
      'orange' => t('Orange'),
    ),
    '#default_value' => variable_get('opt_in_cookie_consent_color_settings_background', 'green'),
    '#description' => t('Choose the color for the background of the bar.'),
  );

  // Library settings.
  $form['library_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rijksoverheid Cookie Opt-in library settings'),
    '#description' => t('Settings for the Rijksoverheid Cookie Opt-in library.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );
  $form['library_settings']['cookie_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cookie settings'),
    '#description' => t('Settings for the cookie.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 0,
  );
  $form['library_settings']['cookie_settings']['opt_in_cookie_consent_cookie_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie name'),
    '#description' => t('The name of the cookie in which the consent is stored.'),
    '#default_value' => variable_get('opt_in_cookie_consent_cookie_name', 'allowcookies'),
    '#weight' => 0,
  );
  $form['library_settings']['cookie_settings']['opt_in_cookie_consent_cookie_accept'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie accept value'),
    '#description' => t('The value that is stored in the cookie if consent is given.'),
    '#default_value' => variable_get('opt_in_cookie_consent_cookie_accept', 'yes'),
    '#weight' => 5,
  );
  $form['library_settings']['cookie_settings']['opt_in_cookie_consent_cookie_deny'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie deny value'),
    '#description' => t('The value that is stored in the cookie if consent is not given.'),
    '#default_value' => variable_get('opt_in_cookie_consent_cookie_deny', 'no'),
    '#weight' => 10,
  );

  // Change preferences settings.
  $form['change_preferences'] = array(
    '#type' => 'fieldset',
    '#title' => t('Change preferences settings'),
    '#description' => t('How visitors can change their cookie preferences.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
  );

  $form['change_preferences']['opt_in_cookie_consent_change_preferences_link'] = array(
    '#type' => 'textarea',
    '#title' => t('Change preferences link'),
    '#description' => t('The link which will be displayed to change your cookie preferences.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_preferences_link', '<a class="cc-change-preferences" href="#" title="change cookie preferences">Change my preferences</a> '),
    '#weight' => 1,
  );

  $form['change_preferences']['opt_in_cookie_consent_change_preferences_callback'] = array(
    '#type' => 'textfield',
    '#title' => t('Callback page to include link'),
    '#description' => t('The link will be placed on this page.'),
    '#default_value' => variable_get('opt_in_cookie_consent_change_preferences_callback', 'cookies'),
    '#weight' => 0,
  );

  return system_settings_form($form);
}
