<?php

/**
 * @file
 * Rijksoverheid Cookie Consent log functions.
 */

/**
 * Log page callback function.
 */
function opt_in_cookie_consent_log($unique_id, $timestamp, $remote_addr) {
  // Check if we need Drupal to log the response.
  if (variable_get('opt_in_cookie_consent_log_enabled', TRUE)) {
    // Write the response data to the log.
    $query = db_insert('opt_in_cookie_consent_log')
      ->fields(array(
        'timestamp' => time(),
        'remote_addr' => $remote_addr,
        'unique_id' => $unique_id,
      ))
      ->execute();
  }

  if (FALSE !== $query) {
    return TRUE;
  }
}
