Description

This module allows your website to comply with the Dutch law on the use of 
cookies. By Dutch law the visitor must consent with placing cookies on your 
client to gather visits or other information about the visitor. The site 
owner must inform the visitor about what kind of information will be 
gathered and must be able to provide proof of visitors’ consent.

The Dutch government provides a javascript-based solution that displays a 
bar on top of the website. This bar provides all necessary elements to 
comply with the law, like asking for permission and providing information, 
except for actual disabling of cookies and tracking scripts. 

This module uses the javascript solution from the Dutch government as a 
library and adds the ability to prevent modules from placing tracking 
cookies on your client. The main module offers the basic functionality 
like showing and configuring the cookie-bar. To exclude certain 
functionalities, submodules are used. Currently there is a submodule 
for Google analytics, Piwik and Webtrends. These submodules make sure 
their javascripts are not being executed, and their cookies are not being 
set, when the visitor has not agreed. Users can always change their choice; 
the module provides a link so visitors can change their mind. If cookies 
were already set at this point, the module will delete them.

All choices made by visitors are logged into the drupal database, providing 
a unique key per visitor. The module provides two hooks, which developers 
can use to build their own submodule. It is very easy to extend the 
functionality of this module, one can just take one of the submodules as an 
example and extend the module with their functionality. 

This module is built in association with Dictu, and is maintained by Sogeti.

Installation
- Download & Install the module
- Download & add the Rijksoverheid cookie opt-in library: 
http://www.rijksoverheid.nl/documenten-en-publicaties/brochures/
2012/11/22/download-rijksoverheid-cookie-opt-in-zip.html

Put it in the folder: libraries/rijksoverheid_cookie_opt-in,
make sure the library has this exact name.
- Enable the main module and the submodules you want.
- Change texts as you like
- Done!
